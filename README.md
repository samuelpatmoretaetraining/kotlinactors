
### What is this repository for? ###

Simple Android application written in Kotlin to display information about actors.

One day project to test proficency creating a simple application using Kotlin. Ain is display Actors in a simple list view, which navigate to a more details page.

* Kotlin language support
* Fragment views in MVP pattern
* Dagger for dependency injection
* Rx, OkHttp & Retrofit for data retreival
* Unit tests using JUnit Mockito

Version 0.1

Min Android 23.0.0

### How do I get set up? ###

Down load source code and open in Android Studio, build and run on a device or android emulator with API above .

### Created by Samuel Patmore. ###